﻿using StudentsListeners.Code;

namespace StudentsListeners;

public static class Program
{
    public static async Task Main(string[] args)
    {
        var app = new App();
        await app.Run();
    }
}