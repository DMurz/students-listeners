﻿namespace StudentsListeners.Code;

public class Classroom<T>
{
    public ITeacher<T> Teacher { get; }
    public List<IStudent<T>> Students { get; }

    public Classroom(ITeacher<T> teacher)
    {
        Teacher = teacher;
        Students = new List<IStudent<T>>();
    }

    public Classroom<T> AddStudent(IStudent<T> student)
    {
        Students.Add(student);
        Teacher.OnSpeak += student.Hear;
        return this;
    }

    public Classroom<T> RemoveStudent(IStudent<T> student)
    {
        Students.Remove(student);
        Teacher.OnSpeak -= student.Hear;
        return this;
    }
}