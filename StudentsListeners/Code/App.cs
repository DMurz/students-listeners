﻿namespace StudentsListeners.Code;

public class App
{
    private readonly Classroom<string> _classroom;

    public App() =>
        _classroom =
            new Classroom<string>(new Teacher<string>())
                .AddStudent(new Student<string>("Petya"))
                .AddStudent(new Student<string>("Kolya"))
                .AddStudent(new Student<string>("Vasya"))
                .AddStudent(new Student<string>("Dima"))
                .AddStudent(new Student<string>("Vanya"));

    public async Task Run()
    {
        var iterator = 0;
        while (true)
        {
            _classroom.Teacher.Speak($"Info {iterator}");
            iterator++;
            await Task.Delay(1000);
        }
    }
}