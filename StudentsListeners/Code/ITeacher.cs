﻿namespace StudentsListeners.Code;

public interface ITeacher<T>
{
    public event Action<T> OnSpeak;
    public void Speak(T info);
}