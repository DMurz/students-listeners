﻿namespace StudentsListeners.Code;

public class Student<T> : IStudent<T>
{
    private readonly string _name;

    public Student(string name) => 
        _name = name;

    public void Hear(T info) => 
        Console.WriteLine($"Student: {_name} hear info: {info}");
}