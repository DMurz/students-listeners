﻿namespace StudentsListeners.Code;

public interface IStudent<T>
{
    public void Hear(T info);
}