﻿namespace StudentsListeners.Code;

public class Teacher<T> : ITeacher<T>
{
    public event Action<T>? OnSpeak;
    
    public void Speak(T info) => 
        OnSpeak?.Invoke(info);
}